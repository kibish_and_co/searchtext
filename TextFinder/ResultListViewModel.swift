//
//  ResultListViewModel.swift
//  TextFinder
//
//  Created by Nick Kibish on 1/23/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import Foundation

protocol ResultListViewModelDelegate {
    func changed(progress: Float, at index: Int)
    func addedNewItem(at index: Int)
    func changedState(at index: Int)
}

class ResultListViewModel {
    var allPages = [WebPage]()
    var loadingThreads = [WebPage]()
    var searchParameters = SearchParameters()
    
    init(searchParameters: SearchParameters) {
        self.searchParameters = searchParameters
    }
    
    init() {
        
    }
    
    fileprivate func addFirstPageToDownload() {
        let page = WebPage(url: searchParameters.startURL)
        allPages.append(page)
        page.loadPage()
    }
    
    fileprivate updateLoading() {
    
    }
}

// MARK: - View Parameters of cell
extension ResultListViewModel {
    func progressViewHidden(at indexPath: IndexPath) -> Bool {
        return false
    }
    
    func progress(at indexPath: IndexPath) -> Float {
        return 0.1
    }
    
    func statusLabelText(at indexPath: IndexPath) -> String {
        return ""
    }
}
