//
//  ResultListTableViewController.swift
//  TextFinder
//
//  Created by Nick Kibish on 1/21/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import UIKit

struct SearchParameters {
    let maxThreadCount: Int
    let maxURLCount: Int
    let startURL: URL
    let searchText: String
    
    init() {
        maxURLCount = 4
        maxThreadCount = 4
        startURL = URL(string: "https://www.raywenderlich.com")!
        searchText = "class"
    }
}

class ResultListTableViewController: UITableViewController {
    let viewModel: ResultListViewModel = ResultListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.allPages.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! ResultTableViewCell
        
        cell.statusLabel.text = viewModel.statusLabelText(at: indexPath)
        cell.progressBar.progress = viewModel.progress(at: indexPath)
        cell.progressBar.isHidden = viewModel.progressViewHidden(at: indexPath)
        
        return cell
    }
}

extension ResultListTableViewController: ResultListViewModelDelegate {
    func changed(progress: Float, at index: Int) {
        guard let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ResultTableViewCell else { return }
        cell.progressBar.progress = progress
    }
    
    func addedNewItem(at index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    func changedState(at index: Int) {
        
    }
}
