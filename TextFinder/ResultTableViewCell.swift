//
//  ResultTableViewCell.swift
//  TextFinder
//
//  Created by Nick Kibish on 1/21/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import UIKit

class ResultTableViewCell: UITableViewCell {
    @IBOutlet var progressBar: UIProgressView!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var stopBtn: UIButton!
    @IBOutlet var pauseBtn: UIButton!
}

extension ResultTableViewCell: LoadThreadDelegate {
    internal func loadingCompletedWithError(error: Error) {
        
    }

    func loadingCompleted(textFound: Int, urls: [URL]) {
        progressBar.progress = 1.1
        progressBar.progressViewStyle = .bar
        statusLabel.text = "Found \(textFound) results"
    }
    
    func updated(progress: Float) {
        progressBar.setProgress(progress, animated: true)
    }
    
    func changed(state: LoadThreadState) {
        switch state {
        case .ready:
            progressBar.isHidden = true
            statusLabel.text = "Waiting..."
            stopBtn.isHidden = true
            pauseBtn.isHidden = true
            break
        case .loading:
            progressBar.isHidden = false
            stopBtn.isHidden = false
            pauseBtn.isHidden = false
            break
        case .canceled, .completed:
            progressBar.isHidden = false
            
            stopBtn.isHidden = false
            pauseBtn.isHidden = true
            stopBtn.setTitle("Reload", for: .normal)
            break
        default: break
            
        }
    }
}
