//
//  Queue.swift
//  TextFinder
//
//  Created by Nick Kibish on 1/23/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import Foundation

struct Queue<T> {
    fileprivate var list = LinkedList<T>()
    
    var isEmpty: Bool {
        return list.isEmpty
    }
    
    var count: Int {
        return list.count
    }
    
    mutating func enqueue(_ element: T) {
        list.append(element)
    }
    
    public mutating func dequeue() -> T? {
        guard !list.isEmpty, let element = list.first else { return nil }
        
        _ = list.remove(element)
        
        return element.value
    }
    
    public func peek() -> T? {
        return list.first?.value
    }
}
