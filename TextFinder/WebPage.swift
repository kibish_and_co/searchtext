//
//  WebPage.swift
//  TextFinder
//
//  Created by Nick Kibish on 1/23/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import Foundation

enum WebPageState {
    case ready, loading, parsing, completed, canceled
}

struct WebPage {
    let url: URL
    var foundURLs = [URL]()
    var foundTextCount = 0
    
    var state = WebPageState.ready
    
    init(url: URL) {
        self.url = url 
    }
}
