//
//  LoadThread.swift
//  TextFinder
//
//  Created by Nick Kibish on 1/20/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

// https://www.raywenderlich.com

import Foundation

extension Notification.Name {
    static let loadThreadCompleted = "loadThreadCompleted"
}

protocol LoadThreadDelegate {
    func loadingCompleted(textFound: Int, urls: [URL])
    func loadingCompletedWithError(error: Error)
    func updated(progress: Float)
    func changed(state: LoadThreadState)
}

class LoadThread: Thread {
    let url: URL
    var delegate: LoadThreadDelegate? {
        didSet {
            switch state {
            case .completed, .parsing:
                sendData()
            default:
                break
            }
        }
    }
    
    var urls = [URL]()
    var textCounter = 0
    var state = LoadThreadState.ready
    lazy var task: URLSessionDownloadTask = {
        let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        return session.downloadTask(with: self.url)
    }()
    
    init(url: URL) {
        self.url = url
        super.init()
    }
    
    override func main() {
        super.main()
        task.resume()
    }
    
    func sendData() {
        DispatchQueue.main.async {
            let url = URL(string: "http://www.google.com")!
            self.delegate?.loadingCompleted(textFound: self.textCounter, urls: [url])
        }
    }
    
    func getHTML() throws -> String {
        return try String(contentsOf: url)
    }
    
    func findText(on page: String) -> [String] {
        return []
    }
    
    func findURLs(on page: String) -> [URL] {
        return []
    }
}

extension LoadThread: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        sendData()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let error = error {
            delegate?.loadingCompletedWithError(error: error)
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        var progress: Float = 1.0
        if let response = (downloadTask.response) as? HTTPURLResponse {
            let headers = response.allHeaderFields
            if let contentLength = headers["Content-Length"] as? String {
                if let lenghth = Int(contentLength) {
                    progress = Float(totalBytesWritten) / Float(lenghth)
                }
            }
        }
        
        DispatchQueue.main.async {
            self.delegate?.updated(progress: progress)
        }
    }
}
