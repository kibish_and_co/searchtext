//
//  LinkedList.swift
//  TextFinder
//
//  Created by Nick Kibish on 1/23/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import Foundation

struct LinkedList<T> {
    var head: Node<T>?
    var tail: Node<T>?
    private(set) var count: Int = 0
    
    var first: Node<T>? {
        return head
    }
    
    var isEmpty: Bool {
        return head == nil
    }
    
    mutating func append(_ value: T) {
        let newNode = Node(value: value)
        
        if let tailNode = tail {
            newNode.previous = tailNode
            tailNode.next = newNode
        } else {
            head = newNode
        }
        
        tail = newNode
        count += 1
    }
    
    mutating func remove(_ node: Node<T>) -> T {
        let prev = node.previous
        let next = node.next
        
        if let prev = prev {
            prev.next = next
        } else {
            head = next
        }
        next?.previous = prev
        
        if next == nil {
            tail = prev
        }
        
        node.previous = nil
        node.next = nil
        
        count -= 1
        
        return node.value
    }
    
}
