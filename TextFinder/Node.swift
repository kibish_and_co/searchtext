//
//  Node.swift
//  TextFinder
//
//  Created by Nick Kibish on 1/23/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import Foundation

class Node<T> {
    var value: T
    var next: Node<T>?
    var previous: Node<T>?
    
    init(value: T) {
        self.value = value
    }
}
